﻿using UnityEngine;
using System.Collections;

public class Webcam : MonoBehaviour
{
    WebCamTexture c;

    void Start()
    {
        WebCamDevice[] wcd = WebCamTexture.devices;

        if (wcd.Length == 0)
        {
            print("找不到實體攝影機");
        }
        else
        {
            foreach (WebCamDevice wc in wcd)
            {
                print("目前可用的攝影機有：" + wc.name);
            }
            print("----------------------------------------------------------------");
            print("目前使用的攝影機是：" + wcd[0].name);
            c = new WebCamTexture(wcd[0].name, 1280, 720, 60);
            GetComponent<Renderer>().material.mainTexture = c;
            c.Play();
            GetComponent<Renderer>().material.shader = Shader.Find("Unlit/Texture");
            transform.localScale = new Vector3((float)c.width / c.height, 1);

        }
    }

    void OnDisable()
    {
        if (c != null)
        {
            c.Stop();
        }
    }

    public Texture WebcamTexture
    {
        get
        {
            return c;
        }
    }
}


