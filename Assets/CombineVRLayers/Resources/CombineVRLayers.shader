﻿Shader "Hidden/CombineVRLayers" 
{
	Properties 
	{
        _nearTexture("NearTexture", 2D) = "white" {}
        _farTexture("FarTexture", 2D) = "white" {}
        _webcamTexture("WebcamTexture", 2D) = "white" {}
	}
	
	SubShader 
	{
		//Tags { "RenderType"="Opaque" }		
		//LOD 200
		
		/////////////////////////////////////////测试区
		//Tags{ "Queue" = "Background" }
		//Tags{ "ForceNoShadowCasting"="True" }
		//ZWrite Off
		
		
		Pass
		{
			CGPROGRAM  
            #pragma vertex vert  
            #pragma fragment frag

			#include "UnityCG.cginc"
			
            sampler2D _nearTexture;
            sampler2D _farTexture;
            sampler2D _webcamTexture;
			
			//--------------顶点输入结构体-------------  
            struct vertexInput   
            {  
                float4 vertex : POSITION;  
                float3 normal : NORMAL;  
                float2 tex : TEXCOORD0;
            };  
  
            //--------------顶点输出结构体-------------  
            struct vertexOutput   
            {  
                float4 pos : POSITION;  
                float2 tex : TEXCOORD0;
            };  
            
			vertexOutput vert(vertexInput v)  
            {
	            vertexOutput o;
	            o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
   				o.tex = v.tex;
                return o;
            }
    
			float4 frag(vertexOutput i) : COLOR  
            {
                float4 color = float4(1,1,1,1);//tex2D(_webcamTexture, i.tex);
                return color;
            }
            
			ENDCG
		}
	} 
	FallBack "Diffuse"
}
