﻿using UnityEngine;
using System.Collections;

public class CombineVRLayers : MonoBehaviour
{
    RenderTexture nearTexture, farTexture, destTexture;
    Material combineVRLayersMaterial, postMaterial;
    Camera renderCamera;

    public static CombineVRLayers CombineVRLayersInstance;

    public static CombineVRLayers instance
    {
        get
        {
            if (CombineVRLayersInstance == null)
            {
                CombineVRLayersInstance = GameObject.FindObjectOfType<CombineVRLayers>();
            }
            return CombineVRLayersInstance;
        }
    }

    void Awake()
    {
        var w = Screen.width / 2;
        var h = Screen.height / 2;
        if (nearTexture == null || nearTexture.width != w || nearTexture.height != h)
        {
            nearTexture = new RenderTexture(w, h, 24, RenderTextureFormat.ARGB32);
            nearTexture.antiAliasing = QualitySettings.antiAliasing == 0 ? 1 : QualitySettings.antiAliasing;
        }

        if (farTexture == null || farTexture.width != w || farTexture.height != h)
        {
            farTexture = new RenderTexture(w, h, 24, RenderTextureFormat.ARGB32);
            farTexture.antiAliasing = QualitySettings.antiAliasing == 0 ? 1 : QualitySettings.antiAliasing;
        }

        //postMaterial = new Material(Shader.Find("Unlit/Texture"));
        postMaterial = new Material(Shader.Find("Custom/SteamVR_ColorOut"));
    }

    void Update()
    {
        RenderVRLayers();//Just for test
    }

    Texture GetWebcamTexture()
    {
        return this.GetComponent<Webcam>().WebcamTexture;
    }

    public void RenderVRLayers()
    {
        //Debug.Log("RenderVRLayers Start");

        //New Material
        if (combineVRLayersMaterial == null)
        {
            combineVRLayersMaterial = new Material(Shader.Find("Hidden/CombineVRLayers"));
        }
        combineVRLayersMaterial.SetTexture("_nearTexture", nearTexture);
        combineVRLayersMaterial.SetTexture("_farTexture", farTexture);
        combineVRLayersMaterial.SetTexture("_webcamTexture", GetWebcamTexture());

        //New Render target
        if (destTexture == null)
        {
            destTexture = new RenderTexture(Screen.width, Screen.height, 0, RenderTextureFormat.ARGB32);
            destTexture.Create();
        }

        //New Camera
        if (renderCamera == null)
        {
            GameObject cameraObj = new GameObject("CombineVRLayers_DrawPostprocessingCamera" + Random.seed);
            renderCamera = cameraObj.AddComponent<Camera>();
        }
        renderCamera.backgroundColor = Color.black;
        renderCamera.orthographic = true;
        renderCamera.orthographicSize = 5;

        //Set render target
        RenderTexture.active = destTexture;
        renderCamera.targetTexture = destTexture;

        //Start render
        GL.Clear(true, true, new Color(1, 0, 0, 1));   // clear the full RT
        GL.PushMatrix();
        for (int i = 0; i < combineVRLayersMaterial.passCount; i++)
        {
            //Debug.Log("render pass : " + i);

            combineVRLayersMaterial.SetPass(i);
            GL.LoadOrtho();
            GL.Viewport(new Rect(0, 0, destTexture.width, destTexture.height));

            GL.Begin(GL.QUADS);
            GL.TexCoord2(0, 0);
            GL.Vertex3(0.0F, 0.0F, 0);
            GL.TexCoord2(0, 1);
            GL.Vertex3(0.0F, 1.0F, 0);
            GL.TexCoord2(1, 1);
            GL.Vertex3(1.0F, 1.0F, 0);
            GL.TexCoord2(1, 0);
            GL.Vertex3(1.0F, 0.0F, 0);
            GL.End();
        }
        GL.PopMatrix();

        Graphics.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), destTexture, postMaterial);
        //Debug.Log("RenderVRLayers end");
    }

    public RenderTexture NearRT
    {
        get { return nearTexture; }
    }

    public RenderTexture FarRT
    {
        get { return farTexture; }
    }
}
